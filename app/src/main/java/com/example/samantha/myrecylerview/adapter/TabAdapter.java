package com.example.samantha.myrecylerview.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.samantha.myrecylerview.DCFragment;
import com.example.samantha.myrecylerview.MarvelFragment;

public class TabAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public TabAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new MarvelFragment();
            case 1:
                return new DCFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
