package com.example.samantha.myrecylerview.APIHelper;

import com.example.samantha.myrecylerview.model.CategoriesModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoriesItems {

    @SerializedName("categories")
    private List<CategoriesModel> categoriesModelList;

    public CategoriesItems(List<CategoriesModel> categoriesModelList) {
        this.categoriesModelList = categoriesModelList;
    }

    public List<CategoriesModel> getCategoriesModelList() {
        return categoriesModelList;
    }

}
