package com.example.samantha.myrecylerview.model;

import com.google.gson.annotations.SerializedName;

public class FoodsModel {

    @SerializedName("strMeal")
    private String nama_makanan;

    @SerializedName("strCategory")
    private String kategori;

    @SerializedName("strArea")
    private String negara;

    @SerializedName("strMealThumb")
    private String url;

    public FoodsModel(String nama_makanan, String kategori, String negara, String url) {
        this.nama_makanan = nama_makanan;
        this.kategori = kategori;
        this.negara = negara;
        this.url = url;
    }

    public String getNama_makanan() {
        return nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getNegara() {
        return negara;
    }

    public void setNegara(String negara) {
        this.negara = negara;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}