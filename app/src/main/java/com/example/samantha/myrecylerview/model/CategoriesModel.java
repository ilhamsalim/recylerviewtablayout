package com.example.samantha.myrecylerview.model;

import com.google.gson.annotations.SerializedName;

public class CategoriesModel {

    @SerializedName("strCategory")
    private String kategori;

    @SerializedName("strCategoryThumb")
    private String url_image;

    @SerializedName("strCategoryDescription")
    private String deskripsi;

    public CategoriesModel(String kategori, String url_image, String deskripsi) {
        this.kategori = kategori;
        this.url_image = url_image;
        this.deskripsi = deskripsi;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
