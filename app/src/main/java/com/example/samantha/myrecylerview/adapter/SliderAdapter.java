package com.example.samantha.myrecylerview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.samantha.myrecylerview.R;
import com.example.samantha.myrecylerview.model.CategoriesModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoriesModel> categoriesModelList;

    public SliderAdapter(Context mContext, List<CategoriesModel> categoriesModelList) {
        this.mContext = mContext;
        this.categoriesModelList = categoriesModelList;
    }

    @NonNull
    @Override
    public SliderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.layout_slider_image, viewGroup, false);
        SliderAdapter.ViewHolder vHolder = new SliderAdapter.ViewHolder(v);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SliderAdapter.ViewHolder viewHolder, int i) {

        Picasso.get().load(categoriesModelList.get(i).getUrl_image()).into(viewHolder.images_slider);

    }

    @Override
    public int getItemCount() {
        return categoriesModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView images_slider;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            images_slider = itemView.findViewById(R.id.imageViewSlider);

        }
    }
}
