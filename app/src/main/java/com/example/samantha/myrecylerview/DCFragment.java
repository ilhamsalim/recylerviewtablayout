package com.example.samantha.myrecylerview;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samantha.myrecylerview.adapter.DCAdapter;
import com.example.samantha.myrecylerview.model.HeroModel;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class DCFragment extends Fragment {

    List<HeroModel> heroModelList;
    RecyclerView recyclerView;
    DCAdapter marvelAdapter;


    public DCFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dc, container, false);

        //pembuatan objek baru dari class Hero Model
        heroModelList = new ArrayList<>();

        //inisialisasi komponen recylerview
        recyclerView = view.findViewById(R.id.recyclerViewDC);

        //pembuatan objek baru dari class DCAdapter
        marvelAdapter = new DCAdapter(getContext(), heroModelList);

        //konfigurasi recylerview
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);


        //pembuatan array
        String judul[] = {"SUPERMAN", "BATMAN", "WONDER WOMAN", "CAT WOMAN", "AQUAMAN", "FLASH", "GREEN ARROW", "HAWKMAN", "ROBIN"};
        String tahun[] = {"2005", "2006", "2017", "2009", "2018", "2017", "2016", "2013", "2014"};
        int gambar[] = {R.drawable.superman, R.drawable.batman, R.drawable.wonderwoman, R.drawable.catwoman,
                R.drawable.aquaman, R.drawable.flash, R.drawable.greenarrow, R.drawable.hawkman, R.drawable.robin};
        String sinopsis[] = {"Ini film SUPERMAN Baguuusss", "Ini film BATMAN Mantaaaap", "Ini film Wonder Woman lohhh",
                "Ini film Cat Woman hottt", "Ini film Aquaman wapikkk", "Ini film Flash mantuul", "Ini film Green Arrow ijo!!!",
                "Ini film HawkMan agak jadul boss", "Ini film Robin temennya batman"};

        //looping array sesuai judul hero
        for (int count = 0; count < judul.length; count++) {
            HeroModel heroModel = new HeroModel();
            heroModel.setJudul_film(judul[count]);
            heroModel.setTahun(tahun[count]);
            heroModel.setGambar_film(gambar[count]);
            heroModel.setSinopsis(sinopsis[count]);
            heroModelList.add(heroModel);
        }

        //set adapter dari recylerview
        recyclerView.setAdapter(marvelAdapter);

        // Inflate the layout for this fragment
        return view;


    }

}
