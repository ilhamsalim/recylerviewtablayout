package com.example.samantha.myrecylerview.model;

public class HeroModel {
    private String judul_film, tahun, sinopsis;
    private int gambar_film;

    public HeroModel() {
    }

    public HeroModel(String judul_film, String tahun, int gambar_film, String sinopsis) {
        this.judul_film = judul_film;
        this.tahun = tahun;
        this.gambar_film = gambar_film;
        this.sinopsis = sinopsis;
    }

    public String getJudul_film() {
        return judul_film;
    }

    public void setJudul_film(String judul_film) {
        this.judul_film = judul_film;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public int getGambar_film() {
        return gambar_film;
    }

    public void setGambar_film(int gambar_film) {
        this.gambar_film = gambar_film;
    }
}


