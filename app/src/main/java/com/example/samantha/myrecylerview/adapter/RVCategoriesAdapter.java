package com.example.samantha.myrecylerview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samantha.myrecylerview.R;
import com.example.samantha.myrecylerview.model.CategoriesModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RVCategoriesAdapter extends RecyclerView.Adapter<RVCategoriesAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoriesModel> categoriesModelList;

    public RVCategoriesAdapter(Context mContext, List<CategoriesModel> categoriesModelList) {
        this.mContext = mContext;
        this.categoriesModelList = categoriesModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.list_item_foods, viewGroup, false);
        RVCategoriesAdapter.ViewHolder vHolder = new RVCategoriesAdapter.ViewHolder(v);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.nama.setText("" + categoriesModelList.get(i).getKategori());
        viewHolder.deskripsi.setText("" + categoriesModelList.get(i).getDeskripsi());
        viewHolder.negara_asal.setText("");
        Picasso.get().load(categoriesModelList.get(i).getUrl_image()).into(viewHolder.images_categories);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "" + categoriesModelList.get(i).getKategori(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoriesModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama, deskripsi, negara_asal;
        ImageView images_categories;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.tv_nama_makanan);
            deskripsi = itemView.findViewById(R.id.tv_kategori);
            negara_asal = itemView.findViewById(R.id.tv_negara);
            images_categories = itemView.findViewById(R.id.imageViewFood);

        }
    }
}
