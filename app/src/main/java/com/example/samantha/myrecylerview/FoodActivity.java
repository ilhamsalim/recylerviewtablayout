package com.example.samantha.myrecylerview;

import android.os.Bundle;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.samantha.myrecylerview.adapter.TabFoodAdapter;

public class FoodActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabFoodAdapter tabFoodAdapter;
    private TabItem tabFood;
    private TabItem tabCategories;
    private Toolbar toolbar;

    String judul[] = {"Foods", "Categories"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);


        //inisilasisasi komponen layout
        findID();

        //konfigurasi tab layout
        tabFoodAdapter = new TabFoodAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabFoodAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                toolbar.setTitle(judul[tab.getPosition()]);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void findID() {
        tabLayout = findViewById(R.id.tab_layout_food);
        tabFood = findViewById(R.id.tab_foods);
        tabCategories = findViewById(R.id.tab_categories);
        viewPager = findViewById(R.id.view_pager);
        toolbar = findViewById(R.id.toolbar);
    }
}
