package com.example.samantha.myrecylerview;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samantha.myrecylerview.adapter.MarvelAdapter;
import com.example.samantha.myrecylerview.model.HeroModel;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MarvelFragment extends Fragment {

    List<HeroModel> heroModelList;
    RecyclerView recyclerView;
    MarvelAdapter marvelAdapter;


    public MarvelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_marvel, container, false);

        //pembuatan objek baru dari class Hero Model
        heroModelList = new ArrayList<>();

        //inisialisasi komponen recylerview
        recyclerView = view.findViewById(R.id.recyclerViewMarvel);

        //pembuatan objek baru dari class DCAdapter
        marvelAdapter = new MarvelAdapter(getContext(), heroModelList);

        //konfigurasi recylerview
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        //pembuatan array
        String judul[] = {"IRON MAN", "CAPTAIN AMERICA", "HULK", "BLACK PANTHER", "DEADPOOL", "SPIDERMAN", "THOR", "VENOM"};
        String tahun[] = {"2010", "2011", "2007", "2018", "2016", "2018", "2017", "2018"};
        String sinopsis[] = {"Ini film IronMan Baguuusss", "Ini film Captain America Mantaaaap", "Ini film Hulk lohhh",
                "Ini film Black Panther joss", "Ini film Deadpool wapikkk", "Ini film Spiderman mantuul", "Ini film Thor masuk pak ekooo",
                "Ini film Venom masih hangat boss"};
        int gambar[] = {R.drawable.ironman, R.drawable.cptamerica, R.drawable.hulk, R.drawable.blackpanther, R.drawable.deadpool,
                R.drawable.spiderman, R.drawable.thor, R.drawable.venom};

        //looping array sesuai judul hero
        for (int count = 0; count < judul.length; count++) {
            HeroModel heroModel = new HeroModel();
            heroModel.setJudul_film(judul[count]);
            heroModel.setTahun(tahun[count]);
            heroModel.setGambar_film(gambar[count]);
            heroModel.setSinopsis(sinopsis[count]);
            heroModelList.add(heroModel);
        }

        //set adapter dari recylerview
        recyclerView.setAdapter(marvelAdapter);
        marvelAdapter.notifyDataSetChanged();

        // Inflate the layout for this fragment
        return view;


    }

}
