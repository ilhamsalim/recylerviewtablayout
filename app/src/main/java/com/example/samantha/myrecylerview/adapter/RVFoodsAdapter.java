package com.example.samantha.myrecylerview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samantha.myrecylerview.R;
import com.example.samantha.myrecylerview.model.FoodsModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RVFoodsAdapter extends RecyclerView.Adapter<RVFoodsAdapter.ViewHolder> {

    private Context mContext;
    private List<FoodsModel> foodsModelList;

    public RVFoodsAdapter(Context mContext, List<FoodsModel> foodsModelList) {
        this.mContext = mContext;
        this.foodsModelList = foodsModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.list_item_foods, viewGroup, false);
        RVFoodsAdapter.ViewHolder vHolder = new RVFoodsAdapter.ViewHolder(v);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.nama.setText("" + foodsModelList.get(i).getNama_makanan());
        viewHolder.kategori.setText("Kategori Makanan : " + foodsModelList.get(i).getKategori());
        viewHolder.negara_asal.setText("" + foodsModelList.get(i).getNegara());
        Picasso.get().load(foodsModelList.get(i).getUrl()).into(viewHolder.images_food);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "" + foodsModelList.get(i).getNama_makanan(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return foodsModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nama, kategori, negara_asal;
        ImageView images_food;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.tv_nama_makanan);
            kategori = itemView.findViewById(R.id.tv_kategori);
            negara_asal = itemView.findViewById(R.id.tv_negara);
            images_food = itemView.findViewById(R.id.imageViewFood);

        }
    }
}
