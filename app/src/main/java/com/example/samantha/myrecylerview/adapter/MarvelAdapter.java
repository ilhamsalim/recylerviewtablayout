package com.example.samantha.myrecylerview.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.samantha.myrecylerview.DetailHeroesActivity;
import com.example.samantha.myrecylerview.R;
import com.example.samantha.myrecylerview.model.HeroModel;

import java.util.List;

public class MarvelAdapter extends RecyclerView.Adapter<MarvelAdapter.ViewHolder> {
    Context mContext;
    List<HeroModel> heroModelList;

    public MarvelAdapter(Context mContext, List<HeroModel> heroModelList) {
        this.mContext = mContext;
        this.heroModelList = heroModelList;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.list_film_heroes, parent, false);
        MarvelAdapter.ViewHolder vHolder = new MarvelAdapter.ViewHolder(v);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.judul.setText(heroModelList.get(position).getJudul_film());
        holder.tahun.setText(heroModelList.get(position).getTahun());
        holder.image.setImageResource(heroModelList.get(position).getGambar_film());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailHeroesActivity.class);
                intent.putExtra("judul", heroModelList.get(position).getJudul_film());
                intent.putExtra("tahun", heroModelList.get(position).getTahun());
                intent.putExtra("gambar", heroModelList.get(position).getGambar_film());
                intent.putExtra("sinopsis", heroModelList.get(position).getSinopsis());
                v.getContext().startActivity(intent);

                Toast.makeText(mContext, "" + heroModelList.get(position).getJudul_film(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return heroModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView judul, tahun;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            judul = itemView.findViewById(R.id.textView_judul);
            tahun = itemView.findViewById(R.id.textView_tahun);
            image = itemView.findViewById(R.id.imageView);
        }
    }
}
