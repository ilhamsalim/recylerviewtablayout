package com.example.samantha.myrecylerview;

import android.os.Bundle;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.samantha.myrecylerview.adapter.TabAdapter;

public class MainActivity extends AppCompatActivity {
    int logo[] = {R.drawable.marvel, R.drawable.dc};
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabAdapter tabAdapter;
    private TabItem tabMarvel;
    private TabItem tabDC;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //inisilasisasi komponen layout
        findID();

        //konfigurasi tab layout
        tabAdapter = new TabAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                //settings toolbar
                toolbar.setLogo(logo[tab.getPosition()]);
                toolbar.setTitle("");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void findID() {
        tabLayout = findViewById(R.id.tab_layout);
        tabMarvel = findViewById(R.id.tab_marvel);
        tabDC = findViewById(R.id.tab_dc);
        viewPager = findViewById(R.id.view_pager);
        toolbar = findViewById(R.id.toolbar);
    }
}
