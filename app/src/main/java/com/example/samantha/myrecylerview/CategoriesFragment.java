package com.example.samantha.myrecylerview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.samantha.myrecylerview.APIHelper.ApiClient;
import com.example.samantha.myrecylerview.APIHelper.ApiInterface;
import com.example.samantha.myrecylerview.APIHelper.CategoriesItems;
import com.example.samantha.myrecylerview.adapter.RVCategoriesAdapter;
import com.example.samantha.myrecylerview.model.CategoriesModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CategoriesFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<CategoriesModel> categoriesModelList;
    private RVCategoriesAdapter rvCategoriesAdapter;


    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);


        recyclerView = view.findViewById(R.id.recyclerViewCategories);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesItems> call = apiService.getCategories();
        call.enqueue(new Callback<CategoriesItems>() {
            @Override
            public void onResponse(Call<CategoriesItems> call, Response<CategoriesItems> response) {

                categoriesModelList = response.body().getCategoriesModelList();
                rvCategoriesAdapter = new RVCategoriesAdapter(getActivity(), categoriesModelList);
                recyclerView.setAdapter(rvCategoriesAdapter);

            }

            @Override
            public void onFailure(Call<CategoriesItems> call, Throwable t) {
                Log.wtf("hasil", t.getMessage());
            }
        });


        // Inflate the layout for this fragment
        return view;
    }
}
